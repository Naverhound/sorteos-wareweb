const express = require('express');
const fs = require('fs');
const mysql = require('mysql2/promise');
const { Sequelize } = require('sequelize');
const dbConfig= require('./config/config.json');
const boRoutes = require('./routes/backofficeRoutes');//traer router de BackOffice
const indexRoutes=require('./routes/frontendRoutes');//traer router de FrontEnd
const schedule= require('node-schedule');
const scheduled_jobs= require ('./utilities/scheduled_jobs')
const app = express();
var cors = require('cors')
const port = process.env.PORT||3000//process obtiene la variable de ambiente del hosting
app.set('port', port);
//en su defecto si no hay variable port del hosting, asignara el puerto luego del OR
//incorporacion de modulos y configuraciones de la app
app.use(express.json({limit: "50mb"}));
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use('/api',indexRoutes);//uso de router front office
app.use('/api/bo',boRoutes);//uso de router de backoffice
app.use(function(err, req, res, next) {//middleware de manejo de errores
  console.error(err.stack);
  res.status(500).send('Something broke! morro :C');
});
app.use(function(req, res, next) {//middleware 404
  res.status(404).send('No parece haber nada por aqui humm!');
});
//DB configuration
async function setupDB(){
  try {    
    // create db if it doesn't already exist
    //se llenan los elementos del objeto basados la estructura JSon de dbConfig
    const { host, port, user, password, database } = dbConfig.initialconfig;
    const connection = await mysql.createConnection({ host, port, user, password });
    await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\`;`);
    connection.close();
    //connect to the database
    const sequelizeCon = new Sequelize(database, user, password, { dialect: 'mysql' });
    //check connection and handle
      //descomentar para sincronizar la BD a los modelos : 
      /*await sequelizeCon.sync({force:true});//sincronizar a la DB y asi efectuar los cambios 
      console.log('!!!Base de datos Sincronizada!!!!!');*/
    await sequelizeCon.authenticate();
    
    console.log('<<Connection with DB has been established successfully on '+database+'.>>');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

function setupApp(){
  //we start by ensuring that all folders are up to use
  let raffleMediaDir=__dirname+'/media/rafflesMedia';
  if(!fs.existsSync(raffleMediaDir))fs.mkdirSync(raffleMediaDir,{recursive:true});
  //then we define a task to be repeated every certain time
  //that's why we call a function in UTILITIES folder
  schedule.scheduleJob('0/5 * * * * *',()=>{
    scheduled_jobs.RefundTickets();
  });
      /* 
    *    *    *    *    *    *
    ┬    ┬    ┬    ┬    ┬    ┬
    │    │    │    │    │    │
    │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
    │    │    │    │    └───── month (1 - 12)
    │    │    │    └────────── day of month (1 - 31)
    │    │    └─────────────── hour (0 - 23)
    │    └──────────────────── minute (0 - 59)
    └───────────────────────── second (0 - 59, OPTIONAL)*/
}

//definición de puerto para arranque de la app (api)
app.listen(port, (err) => {
  if (err) {
    console.log(err)
    return
   }
  console.log(`<--<--Sorteos WareWeb running at http://localhost:${port}-->-->`);
  setupDB();
  setupApp();
});

//exportación para uso como api y ser llamado a otra parte enviando el objeto app (todo lo actual)
module.exports=app;