const express = require ('express');
const router = express.Router();
const {Auth} = require('../controllers/middlewares');
const {RaffleController,TicketController}= require('../controllers');

router.get('/', (req,res)=>{
    res.send('WareWeb Front with router >:D')
});
router.get('/raffle/:id', (req,res)=>{res.send('alaa la rifa '+req.params.id)});//esta ruta renderizara una vista
//la vista renderizada llamara datos de una rifa específica

//POOOST
router.post('/raffle/reservation',TicketController.create);
router.post('/login',Auth.getAuth);

module.exports = router;

