const express = require('express');
const {UserController,RaffleController}= require('../controllers');
const {TreatFiles,Auth}= require('../controllers/middlewares');
let router= express.Router();

//rutas
//TODAS REQUIEREN PREFIJO DE api/bo/<ruta de aqui></ruta>
//<-<-Users->->->
router.get('/', UserController.index);
router.get('/settings',UserController.settings);
router.route('/users')
  .all(function (req, res, next) {
    // runs for all HTTP verbs first
    // think of it as route specific middleware!
    //aqui basicamente harias que se corran las autenticaciones de loggeo y demas cosas de seguridad en rutas
    next()
  },Auth.checkAuth)
  .get(UserController.get)
  .put(UserController.update)
  .post(UserController.create)
  .delete(UserController.delete)
//<-<-Raffles->->->
router.route('/raffles')
.all(function (req, res, next) {
  // runs for all HTTP verbs first
  // think of it as route specific middleware!
  //aqui basicamente harias que se corran las autenticaciones de loggeo y demas cosas de seguridad en rutas
  next();
},Auth.checkAuth)
.get(RaffleController.get)
.patch(TreatFiles.treat,RaffleController.update)
.post(TreatFiles.treat,RaffleController.create)
.delete(RaffleController.delete)

module.exports= router;