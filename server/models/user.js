'use strict';
const bcrypt= require('bcrypt');
const { Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Raffle,{foreignKey:'userId'});
    }
  };
  User.init({//atributos del modelo son definidos aqui
    uname: {
      type:DataTypes.STRING,
      unique:true,
      allowNull:false,
      validate:{
       notNull:{msg:"Nombre de Usuario es requerido"},
       notEmpty:{msg:"El nombre de usuario no puede ser inexistente"},
       is:{args:[],msg:"Nombre de Usuario debe constar de letras, numeros o los siguientes simbolos:-,_,"},
       isUnique(uname) {
        return User.findOne({where:{uname:uname}})
          .then((uname) => {
            if (uname)throw new Error('Nombre De usuario ya esta en uso');})
      }
      }
    },
    fnames: {
      type:DataTypes.STRING,
      allowNull:false,
      validate:{
        notNull:{msg:"Es necesario el Campo de Nombre"},
        notEmpty:{msg:"Debe Especificar su Nombre(s)"}
      },
      
    },
    lnames: {
      type:DataTypes.STRING,
      allowNull:false,
      validate:{
        notNull:{msg:"Es necesario el Campo de Apellido(s)"},
        notEmpty:{msg:"Debe Especificar su Apellido(s)"}
      }
    },
    email: {
      type:DataTypes.STRING,
      unique:true,
      allowNull:false,
      validate:{
        isEmail:{msg:"Formato de email invalido"},
        isLowercase:{msg:"Email debe estar escrito en minusculass"},
        notNull:{msg:"Es necesario el Campo de email"},
        notEmpty:{msg:"Debe Especificar un correo electronico para el registro"},
        isUnique(email) {
          return User.findOne({where:{email:email}})
            .then((email) => {
              if (email)throw new Error('Este email ya ha sido registrado');
            })
        }
      }      
    },
    password:{ 
      type:DataTypes.STRING,
      allowNull:false,
      validate:{
        is:{args:["^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!_@#\$%\^&\*])(?=.{8,})"],
            msg:"Password de al menos 8 con al menos: 1 minuscula, 1 mayuscula, 1 numero y 1 caracter especial (_ @ # $ % ^ & *)"},
        notNull:{msg:"Es necesario el campo de Password"},
        notEmpty:{msg:"Debe ser especificada una password"}
      }
    },
    role: {
      type:DataTypes.STRING,
      allowNull:false,
      defaultValue:'user'
    }
  }, {
    hooks:{
    //TODO:BeforeCreate y BeforeUpdate deben encriptar la password con 1 misma funcion
    beforeCreate : (user , options) => {
        {
          console.log(user);
          encryptPassword(user,options);
        }
      },
      beforeBulkUpdate:(user,options)=>{
        console.log(user.attributes);
        encryptPassword(user.attributes,options);
      }
    },
    sequelize,//instancia (objeto) de la conexion
    modelName: 'User',//Nombre del modelo de tabla de la DB
  },
  );
  return User;
};

function encryptPassword(user,options) {
  user.password = user.password && user.password != "" ? bcrypt.hashSync(user.password, 10) : "";
  
}