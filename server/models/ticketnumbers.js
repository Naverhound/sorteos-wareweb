'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TicketNumbers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      TicketNumbers.belongsTo(models.Ticket,{foreignKey:'ticketId'})
    }
  };
  TicketNumbers.init({
    ticketId: DataTypes.INTEGER,
    tnumber: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'TicketNumbers',
  });
  return TicketNumbers;
};