'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RaffleMedia extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      RaffleMedia.belongsTo(models.Raffle,{foreignKey:'raffleId'});
    }
  };
  RaffleMedia.init({
    raffleId: {
      type:DataTypes.INTEGER,
      allowNull:false,
      validate:{
        notNull:{msg:"Identificador de Rifa no recibido"},
        notEmpty:{msg:"Identificador de Rifa en blanco/sin valor"},
        isNumeric:{msg:"El identificacion de Rifa no ha sido numerico"}
      }
    },
    mediaroute: {
      type:DataTypes.STRING,
      allowNull:false,
      validate:{
        notNull:{msg:"Ruta de Archivo no recibida"},
        notEmpty:{msg:"Ruta de archivo en blanco/sin valor"},
      }
    }
  }, {
    sequelize,
    modelName: 'RaffleMedia',
  });
  return RaffleMedia;
};