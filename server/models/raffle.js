'use strict';
const {Model} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Raffle extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Raffle.belongsTo(models.User,{as:'Owner', foreignKey:'userId'});
      Raffle.hasMany(models.RaffleMedia,{foreignKey:'raffleId'});
      Raffle.hasMany(models.Discounts,{foreignKey:'raffleId'})
    }
  };
  Raffle.init({
    userId: {
      type:DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull:{msg:"Identificador de usuario no especificado"},
        notEmpty:{msg:"Identificador de usuario en blanco/sin valor"},
        isNumeric:{msg:"El identificacion de usuario no ha sido numerico"}
        
      }
    },
    raffledate: {
      type:DataTypes.DATE,
      allowNull: false,
      validator: {
        notNull: {msg:"Campo de fecha no especificado"},
        notEmpty:{msg:"Fecha no Definida"},
        isDate:{msg:"Formato de Fecha innadecuado"},
        isAfter:{args:DataTypes.NOW,msg:"La rifa ha de relizarse al menos 1 dia despues de la fecha actual"}
      }
    },
    tlreservation:{
      type: DataTypes.INTEGER,
      allowNull: false,
      validator: {
        notNull: {msg:"Se espera un limite de tiempo de apartado para los boletos"},
        notEmpty:{msg:"Limite de tiempo de apartado vacio/sin valor"},
        isInt:{msg:"Se admiten solo numeros  de horas enteros para definir el limite de apartado"}
        
      }
    },
    title: {
      type:DataTypes.STRING,
      allowNull:false,
      validator: {
        notNull: {msg:"Campo de titulo no especificado"},
        notEmpty:{msg:"Descripción vacía"}
      }
    },
    description: {
      type:DataTypes.TEXT,
      allowNull:false,
      validator: {
        notNull: {msg:"Campo de descripción no especificado"},
        notEmpty:{msg:"Descripción vacía"}
      }
    },
    discounts:{
      type:DataTypes.BOOLEAN,
      allowNull:false,
      defaultValue:0,
      validator: {
        isNumeric:{msg:"Valor innadecuado para definir si existen o no descuentos"},
        isIn:{arg:[[0,1]],msg:"Valor incorrecto para activar/desactivar descuentos"},
        notEmpty:{msg:"La confirmacion o negacion de descuentos no puede estar vacia"},
        notNull:{msg:"No se encontro confirmacion o negacion de descuentos para la rifa"}
      }
    },
    chances:{
      allowNull:false,
      defaultValue:1,
      type:DataTypes.INTEGER,
      validate:{
        min:{args:1,msg:"Oportunidades mínimas: 1"},
        max:{args:4,msg:"Oportunidades máximas: 4"},
        isInt:{msg:"La cantidad de oportunidades debe ser un Numero entre 1 y 4 (1 por defecto al no especificar oportunidades)"},
        notNull:{msg:"Campo de Oportunidades no especificado"},
        notEmpty:{msg:"Oportunidades sin valor/en blanco"}
      }
    },
    tcost: {
      type:DataTypes.DECIMAL(10,2),
      allowNull:false,
      validate: {
        notNull:{msg:"El costo de boleto no fue especificado"},
        notEmpty:{msg:"Costo de boleto sin valor/en blanco"},
        isNumeric:{msg:"Numero Proporcionado no tiene el formato adecuado"}
      }
    },
    tavailables:{
      type:DataTypes.INTEGER,
      allowNull:false,
      validate:{
        notNull:{msg:"No se especificó un numero de boletos para la rifa"},
        notEmpty:{msg:"No puede quedarse en blanco el numero de boletos"},
        isInt:{msg:"Formato de numero de boletos innadecuado"}
      }
    }
  }, {
    sequelize,
    modelName: 'Raffle',
  });
  return Raffle;
};