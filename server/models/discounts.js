'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Discounts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Discounts.belongsTo(models.Raffle,{foreignKey:'raffleId'})
    }
  };
  Discounts.init({
    raffleId:{
      type: DataTypes.INTEGER,
      allowNull:false
    },
    numberquantity: {//cuantos numeros a la vez
      type:DataTypes.INTEGER,
      allowNull:false
    },
    cost: {
      type:DataTypes.DECIMAL(10,2)
    }
  }, {
    sequelize,
    modelName: 'Discounts',
  });
  return Discounts;
};