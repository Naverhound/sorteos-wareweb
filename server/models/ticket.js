'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Ticket extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //this.belongsTo(models.User);
      this.belongsTo(models.Raffle);
      this.hasMany(models.TicketNumbers);
    }
  };
  Ticket.init({
    /*userId: {
      type:DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notNull:{msg:"Identificador de usuario no recibido"},
        notEmpty:{msg:"Identificador de usuario en blanco/sin valor"},
        isNumeric:{msg:"El identificacion de usuario no ha sido numerico"}
        
      }
    }*/
    fnames:{
      type:DataTypes.STRING,
      allowNull: false,
    },
    lnames:{
      type:DataTypes.STRING,
      allowNull: false,
    },
    cellphone:{
      type:DataTypes.STRING,
      allowNull: false,
      validate: {
        is:{args:"^(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$",
            msg:"Formato de numero nacional (Mexico): 123-456-7890 (numero con lada)"},
      }
    },
    state:{
      /* 32 estados de mexico:
      Aguascalientes, Baja California, Baja California Sur, 
      Campeche, Coahuila, Colima, Chiapas, Chihuahua,
      Durango, Ciudad de Mexico, Guanajuato, Guerrero,
      Hidalgo, Jalisco, México, Michoacán, Morelos, Nayarit,
      Nuevo León, Oaxaca, Puebla, Querétaro, Quintana Roo, 
      San Luis Potosí, Sinaloa, Sonora, Tabasco, 
      Tamaulipas, Tlaxcala, Veracruz, Yucatán, y Zacatecas*/
      type:DataTypes.STRING,
      allowNull: false,
    },
    raffleId: {
      type:DataTypes.INTEGER,
      allowNull:false,
      validate:{
        notNull:{msg:"Identificador de Rifa no recibido"},
        notEmpty:{msg:"Identificador de Rifa en blanco/sin valor"},
        isNumeric:{msg:"El identificacion de Rifa no ha sido numerico"}
      }
    }
  }, {
    hooks:{
      beforeCreate: (ticket,options)=>{
         
      }
    },
    sequelize,
    modelName: 'Ticket',
  });
  return Ticket;
};