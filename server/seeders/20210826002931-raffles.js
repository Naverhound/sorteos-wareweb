'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('Raffles', [
      {
          userId: '2',
          raffledate: '2021-10-15 06:30:00',
          tlreservation:72,
          title:'Kiandas Haciendo',
          description: 'Rifa del camioneton',
          discounts:0,
          chances:4,
          tcost:250.50,
          tavailables:250,
          createdAt: new Date(),
          updatedAt: new Date()
      },
    {
          userId: '3',
          raffledate: '2021-12-18 13:25:00',
          tlreservation:24,
          title:'Una motillo Yamaha',
          description: 'Rifa de la motonga Yamaha por el naver',
          discounts:true,
          chances:2,
          tcost:250.50,
          tavailables:250,
          createdAt: new Date(),
          updatedAt: new Date()
    }
    ]);

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('Raffles', null, {});
  }
};
