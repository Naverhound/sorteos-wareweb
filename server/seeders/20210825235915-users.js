'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('Users', [
      {
        uname:'john1',
        fnames: 'John',
        lnames: 'Doe',
        email: 'example@example.com',
        password:'lapassword',
        role:'master',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uname: 'john2',
        fnames: 'John2',
        lnames: 'Doe2',
        email: 'example2@example.com',
        password:'lapassword2',
        role:'admin',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uname: 'naverhound',
        fnames: 'carlos',
        lnames: 'perez',
        email: 'carlospmoncada@example.com',
        password:'lapassword',
        role:'',
        createdAt: new Date(),
        updatedAt: new Date()
      }
  ]);

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('Users', null, {});
  }
};
