const {User,Raffle,RaffleMedia} = require('../models');// entre corchetes los nombres definidos con propiedad "modelName"
const bcrypt= require('bcrypt');
const fs=require('fs');
const path = require('path');
const formidable = require('formidable');
const {to,SucRes,ErRes,TE}= require('../utilities/standarizedResponses');
const uploadDirectory=path.join(__dirname,"../media/rafflesMedia");

module.exports.create = async (req,res)=>{
    let fields={};
    let filePaths=[];
    let formParser = formidable({ multiples: true,encoding:'utf-8'});
    
    formParser.parse(req)
        .on('progress', (bytesReceived, bytesExpected) => {
            percent=(bytesReceived*100)/bytesExpected;
            console.log(percent);
        })
        .on('fileBegin', (name, file) => {
            //TODO: validate all files, formats and return any error
            //if(!folderExist) return ErRes(res,'No existe la locacion',422);
            file.path=uploadDirectory+'/'+''+file.name+'algo';
            filePaths.push(file.path);
        })
        .on('field', (name, value) => {
            fields[name]=value;
        })
        .on('error', err => {
            
             return ErRes(res,err,422);
        })
        .on('aborted',()=>{

            console.log(filePaths);
        })
        .on('end', async ()=> {
            [err,raffleCreated]= await to (Raffle.create(
                {
                    userId:fields.userId,
                    raffledate:fields.raffledate,
                    tlreservation:fields.tlreservation,
                    title:fields.title,
                    description:fields.description,
                    chances:fields.chances,
                    discounts:fields.discounts,
                    tcost:fields.tcost,
                    tavailables:fields.tavailables 
                }));
            
            if (err) {//start erasing media related to thad raffle
                err= await unlinkFiles(filePaths,err);
                return ErRes(res,err,422);                 
                }
            if(fields.discounts==1){//TODO://store discounts related to a raffle
                fields.discspec.forEach(async element => {
                    [err,dicountsCreated]=await to(Discounts.create({
                        raffleId:raffleCreated.id,
                        numberquantity:element.nquantity,
                        cost:element.cost
                    }));
                    if(err) {
                        Raffle.destroy({where:{id:raffleCreated.id}});    
                        return ErRes(res,err,422)
                    };
                });
            }

            filePaths.forEach(async path => {
                [err,mediaPathCreated]=await to (RaffleMedia.create({
                    raffleId:raffleCreated.id,
                    mediaroute:path
                }));
                if (err) return ErRes(res,err,422);
            });
            return SucRes(res,{msg:'Rifa Registrada',
                                    raffle:raffleCreated},201);
        });
}

module.exports.get =async (req,res)=>{
    //revisar si hay Parametros Query
    
    if(Object.keys(req.query).length !== 0){
        [err,Queryedraffle]= await to (Raffle.findOne({where:{id:req.query.id},include:[
            {model:User,as:'Owner'},
            {model:RaffleMedia}
        ]}));
        if(err) return ErRes(res,err,422);
        return SucRes(res,{'raffle':Queryedraffle},201);
    }
    //al no haber, continuamos con normalidad
    [err,Queryedraffles]= await to (Raffle.findAll({include:[
        {model:User,as:'Owner'},
        {model:RaffleMedia}
    ]}));
    if(err) return ErRes(res,err,422);
    
    return SucRes(res,{'raffles':Queryedraffles},201);  
}

module.exports.update = async(req,res)=>{
    let raffleId=req.query.id;
    //verificar que userId contenga algo de QueryParameter "uri?parametro+valor&otroparam=otrovalor"
    if(req.is("application/x-www-form-urlencoded")){
    if(!raffleId) return res.json('Usuario a actualizar no especificado');
    [err,userToUpdate]= await to (Raffle.update(req.body,{where:{id:raffleId}}));
    if(err) return ErRes(res,err,422); 
    return SucRes(res,{"msg":"Update Exitoso",
                        "user":userToUpdate},201);  
    }
     
}

module.exports.delete = async (req, res)=>{
    if(!req.query.id) return ErRes(res,'No se Especificó la rifa a eliminar',422);
    [err,rifaBorrada]= await to (Raffle.destroy({where:{id:req.query.id}}));
    if(rifaBorrada==0) return ErRes(res,'No se encontró la Rifa especificada para eliminar',200);
    return SucRes(res,{"msg":"Se elimino con exito","Rifa Eliminada":rifaBorrada},201);
}

async function unlinkFiles (filePaths,err){
    filePaths.every( path => {
        try {
            fs.unlinkSync(path);
        } catch (error) {
            console.log(error);
            err['message']=err.message+"\n Error al eliminar archivos del servidor"; 
            return err;
        }        
    });
    return err;
}