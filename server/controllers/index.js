const UserController= require('./UserController');
const RaffleController= require('./RaffleController');
const TicketController= require('./TicketController');
module.exports = {
    UserController: UserController,
    RaffleController: RaffleController,
    TicketController:TicketController
  }