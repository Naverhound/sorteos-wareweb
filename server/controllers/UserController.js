const {User,Raffle} = require('../models');// entre corchetes los nombres definidos con propiedad "modelName"
const bcrypt= require('bcrypt');
const formidable = require('formidable');
const {to,SucRes,ErRes,TE}= require('../utilities/standarizedResponses');

module.exports.index= async (req, res) =>{
    return SucRes(res,{"msg":"WareWeb BackOffice"},201)
}

module.exports.create = async (req,res)=>{
    let formParser = formidable({ multiples: false,encoding:'utf-8'});
    if(req.is("application/x-www-form-urlencoded")){
        [err,userCreated]= await to (User.create(req.body));
        if (err) return ErRes(res,err,422);
        return SucRes(res,{'msg':'Usuario Registrado',
                            'user':userCreated},201);
    }
    formParser.parse(req, async (err,fields,files)=>{
        
        [err,userCreated]= await to (User.create(fields));
        if (err) return ErRes(res,err,422);
        return SucRes(res,{'msg':'Usuario Registrado',
                            'user':userCreated},201);
    });
}

module.exports.get =async (req,res)=>{
    //revisar si hay Parametros Query
    if(Object.keys(req.query).length !== 0){
        [err,Queryeduser]= await to (User.findOne({where:{id:req.query.id},include:Raffle}));
        if(err) return ErRes(res,err,422);
        return SucRes(res,{'user':Queryeduser},201);
    }
    //al no haber, continuamos con normalidad
    [err,Queryedusers]= await to (User.findAll({include:Raffle}));
    if(err) return ErRes(res,err,422);
    
    return SucRes(res,{'users':Queryedusers},201);  
}

module.exports.update = async(req,res)=>{
    let userId=req.query.id;
    //verificar que userId contenga algo de QueryParameter "uri?parametro+valor&otroparam=otrovalor"
    if(!userId) return res.json('Usuario a actualizar no especificado');
    if(req.is("application/x-www-form-urlencoded")){
        [err,userToUpdate]= await to (User.update(req.body,{where:{id:userId}}));
        if(err) return ErRes(res,err,422); 
        return SucRes(res,{"msg":"Update Exitoso",
                            "user":userToUpdate},201); 
    }
      
}

module.exports.delete = async (req, res)=>{
    if(!req.query.id) return ErRes(res,'No se Especificó el usuario a eliminar',422);
    [err,usuarioBorrado]= await to (User.destroy({where:{id:req.query.id}}))
    return SucRes(res,{"msg":"Se elimino con exito","usuarioBorrado":usuarioBorrado},201);
}

module.exports.settings=async(req,res)=>{
    return SucRes(res,{"msg":"En settings del BackOffice B^)"},201);
}
