const {Raffle,Ticket,TicketNumbers} = require('../models');
const {to,SucRes,ErRes,TE}= require('../utilities/standarizedResponses');

module.exports.create= async (req, res)=>{
  //  return SucRes(res,{msg:'eres el mejor, esta funcionando'},201);
  //parseando directamente una "application/x-www-form-urlencoded"
   if(!req.body.raffleId) return ErRes(res,'No se especificó una rifa',421);
   [err,pass]=await checkFair(req,res);
   if(err) return ErRes(res,err,422);
   if(!pass) return SucRes(res,{msg:'Posee demasiados numeros de la rifa'},200);
   
   [err,folioCreated]= await to(Ticket.create({
     fnames:req.body.fnames,
     lnames:req.body.lnames,
     cellphone:req.body.cellphone,
     state:req.body.state,
     raffleId:req.body.raffleId
   }));
   if(err) return ErRes(res,err,422);
   req.body.numbers.forEach(async number => {
    [err,nStored]= await to(TicketNumbers.create({ticketId:folioCreated.id,tnumber:number}));
    if(err) return ErRes(res,err,422);
   });
   
   return SucRes(res,{msg:"Boletos apartados",folio:folioStored},201);
}

module.exports.update=async (req,res)=>{
  //TODO: proceso de update del folio para pasar de : Apartado a Confirmado en "Status"
}

async function checkFair(req){
  [err,raffleObj]= await to (Raffle.findOne({//will bring one specific object
                              where:{
                                id:req.body.raffleId
                              },
                              attributes:['chances']}));
  if(err) return [err,false];
  if(!raffleObj) return[ 'Rifa no existe',false];
  [err,folios]=await to(
    Ticket.findAll({//will generate an array of objects
            where:{
              cellphone:req.body.cellphone
            },
            attributes:['id']//traer folios relacionados a 1 usuario
            })
          );
  if(err) return [err,false];
  console.log(folios);
  if(!folios.length) return [null,true];
  [err,entries]= await to(
    TicketNumbers.count({
      where:{ticketId:folios.id}
    })
  );
  if(err) return [err,false];
  if(entries==0) return [null,true];
  //if(entries<10) return [null,true];
  switch (raffleObj.chances) {
    case 1:case 2:
      if(entries<10) return [null,true];
      break;
    case 3:
      if(entries<9) return [null,true];
      break;
    case 4:
      if(entries<8) return [null,true];
      break;
    default:
      break;
  }
  return [null,false];
}