'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Raffles', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      raffledate: {//format (YYYY-MM-DD HH-MM-SS(?) -/+0000/-/+00:00)= 2020-12-15 06:30:20 -0600/-06:00 zona horaria al final
        type: Sequelize.DATE,
        allowNull:false
      },
      tlreservation:{//hrs that thickets can remain reserved
        type:Sequelize.INTEGER,
        allowNull:false
      },
      title:{
        allowNull: false,
        type: Sequelize.STRING//TODO: not working, letting me insert with no description at all
      },
      description:{
        allowNull: false,
        type: Sequelize.TEXT//TODO: not working, letting me insert with no description at all
      },
      discounts:{//this boolean is 0|1 only tiny int 1=true
        defaultValue:0,
        type: Sequelize.BOOLEAN
      },
      chances:{
        allowNull:false,
        type:Sequelize.INTEGER
      },
      tcost: {
        allowNull: false,
        type: Sequelize.DECIMAL(10,2)
      },
      tavailables: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Raffles');
  }
};