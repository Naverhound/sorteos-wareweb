'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Tickets', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },/*
      userId: {
        allowNull: false,
        type: Sequelize.INTEGER
      },*/
      fnames:{
        allowNull: false,
        type: Sequelize.STRING
      },
      lnames:{
        allowNull: false,
        type: Sequelize.STRING
      },
      cellphone:{
        allowNull: false,
        type: Sequelize.STRING
      },
      state:{//stands for state where the client is from
        type: Sequelize.STRING,
        allowNull: false
      },
      raffleId: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      status:{
        type: Sequelize.ENUM('Apartado','Confirmado'),
        defaultValue:'Apartado'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Tickets');
  }
};